﻿using Ninject;
using Atlas.TradeManager.Application.AppService;
using Atlas.TradeManager.Application.Interface.AppService;
using Atlas.TradeManager.Application.Interface.Task;
using Atlas.TradeManager.Application.Tasks;
using Atlas.TradeManager.Domain.Interfaces.Repositories;
using Atlas.TradeManager.Domain.Interfaces.Services;
using Atlas.TradeManager.Domain.Services;
using Atlas.TradeManager.Infra.Data.Repositories;
using Atlas.TradeManager.Services;
using Atlas.TradeManager.Services.Interface;

namespace Atlas.TradeManager.CrossCutting.IoC
{
    public static class BootStrapper
    {
        public static void RegisterServices(IKernel container)
        {

            #region AppService
            container.Bind<ICotacaoMoedaAppService>().To<CotacaoMoedaAppService>();
            container.Bind<IExchangeAppService>().To<ExchangeAppService>();
            container.Bind<ITradeAppService>().To<TradeAppService>();
            container.Bind<IUsuarioAppService>().To<UsuarioAppService>();
            container.Bind<IOrdemAppService>().To<OrdemAppService>();
            container.Bind<IHistoricoOrdemAppService>().To<HistoricoOrdemAppService>();
            container.Bind<IParametroAplicacaoAppService>().To<ParametrosAplicacaoAppService>().InTransientScope();
            #endregion

            #region Service
            container.Bind<IExchangeService>().To<ExchangeService>();
            container.Bind<ITradeService>().To<TradeService>();
            container.Bind<IUsuarioService>().To<UsuarioService>();
            container.Bind<IOrdemService>().To<OrdemService>();
            container.Bind<IHistoricoOrdemService>().To<HistoricoOrdemService>();
            container.Bind<IParametrosAplicacaoService>().To<ParametrosAplicacaoService>().InTransientScope();
            #endregion

            #region repository
            container.Bind<ITradeRepository>().To<TradeRepository>();
            container.Bind<IExchangeRepository>().To<ExchangeRepository>();
            container.Bind<IUsuarioRepository>().To<UsuarioRepository>();
            container.Bind<IOrdemRepository>().To<OrdemRepository>();
            container.Bind<IHistoricoOrdemRepository>().To<HistoricoOrdemRepository>();
            container.Bind<IParametrosAplicacaoRepository>().To<ParametrosAplicacaoRepository>().InTransientScope();
            #endregion

            #region task
            container.Bind<IExchangeTask>().To<ExchangeTask>().InSingletonScope();
            container.Bind<ICotacaoMoedaTask>().To<CotacaoMoedaTask>().InSingletonScope();
            container.Bind<IOrdemTask>().To<OrdemTask>().InSingletonScope();
            #endregion

            #region External Services
            container.Bind<IExternalService>().To<ExternalService>();
            #endregion
        }
    }
}
