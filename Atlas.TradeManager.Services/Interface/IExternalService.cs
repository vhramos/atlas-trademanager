﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Entities;

namespace Atlas.TradeManager.Services.Interface
{
    public interface IExternalService
    {
        bool EfetuarRequisicaoHttp(RequisicaoHttp requisicao, out string respostaHttp);
    }
}
