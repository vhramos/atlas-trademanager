﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using Atlas.TradeManager.CrossCutting.Extensions;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Services.Interface;

namespace Atlas.TradeManager.Services
{
    public class ExternalService : IExternalService
    {
        public const string JSON_CONTENT_TYPE = "application/json";
        public bool EfetuarRequisicaoHttp(RequisicaoHttp requisicao, out string respostaHttp)
        {
            try
            {
                var client = new RestClient(requisicao.GatewayUrl);
                var request = new RestRequest(requisicao.RotaApi, requisicao.Metodo);
                if (requisicao.ParametrosQuery.Count > 0)
                {
                    foreach (var parametro in requisicao.ParametrosQuery)
                        request.AddQueryParameter(parametro.Key, parametro.Value);
                }
                if (requisicao.Parametros.Count > 0)
                {
                    foreach (var parametro in requisicao.Parametros)
                        request.AddParameter(parametro.Key, parametro.Value);
                }
                if (requisicao.Headers.Count > 0)
                {
                    foreach (var header in requisicao.Headers)
                        request.AddHeader(header.Key, header.Value);
                }

                if (requisicao.JsonBody != null)
                    request.AddJsonBody(requisicao.JsonBody);

                if (requisicao.Credencial != null)
                    request.Credentials = requisicao.Credencial;

                request.OnBeforeDeserialization = resp => resp.ContentType = JSON_CONTENT_TYPE;
                var response = client.Execute(request);
                respostaHttp = response.Content;
                return response.IsSuccessful;
            }
            catch (Exception erro)
            {
                respostaHttp = erro.Message;
                return false;
            }
        }
    }
}
