﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Infra.Data.EntityConfig;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Entities.Exchanges;
using Microsoft.EntityFrameworkCore;

namespace Atlas.TradeManager.Infra.Data.Contexto
{
    public class TradeManagerContext : DbContext
    {
        public DbSet<Exchange> Exchanges { get; set; }
        public DbSet<Trade> Trades { get; set; }
        public DbSet<ParametrosAplicacao> ParametrosAplicacao { get; set; }
        public DbSet<Ordem> Ordem { get; set; }
        public DbSet<HistoricoOrdem> HistoricoOrdem { get; set; }
        public TradeManagerContext()
        {
        }
        public TradeManagerContext(DbContextOptions<TradeManagerContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ExchangeConfiguration());
            modelBuilder.ApplyConfiguration(new TradeConfiguration());
            modelBuilder.ApplyConfiguration(new ParametrosAplicacaoConfiguration());
            modelBuilder.ApplyConfiguration(new UsuarioConfiguration());
            modelBuilder.ApplyConfiguration(new OrdemConfiguration());
            modelBuilder.ApplyConfiguration(new HistoricoOrdemConfiguration());
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseLazyLoadingProxies();
        }
    }
}
