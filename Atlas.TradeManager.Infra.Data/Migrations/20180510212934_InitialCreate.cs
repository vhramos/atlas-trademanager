﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Atlas.TradeManager.Infra.Data.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Exchanges",
                columns: table => new
                {
                    ExchangeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ApiKey = table.Column<string>(nullable: true),
                    ApiPassword = table.Column<string>(nullable: true),
                    ApiSecret = table.Column<string>(nullable: true),
                    BlinkTradeBokerId = table.Column<string>(nullable: true),
                    ConsultarOrdemResourcePath = table.Column<string>(nullable: true),
                    CriarOrdemResourcePath = table.Column<string>(nullable: true),
                    ExchangeOnline = table.Column<bool>(nullable: false),
                    GatewayUrl = table.Column<string>(nullable: true),
                    IntervaloAtualizacao = table.Column<int>(nullable: false),
                    JsonAskName = table.Column<string>(nullable: true),
                    JsonBidName = table.Column<string>(nullable: true),
                    JsonLastName = table.Column<string>(nullable: true),
                    JsonOrderBookAmountPath = table.Column<string>(nullable: true),
                    JsonOrderBookPricePath = table.Column<string>(nullable: true),
                    JsonVolumnName = table.Column<string>(nullable: true),
                    NomeExchange = table.Column<string>(nullable: true),
                    OrderBookJsonPath = table.Column<string>(nullable: true),
                    OrderBookResourcePath = table.Column<string>(nullable: true),
                    SaldoResourcePath = table.Column<string>(nullable: true),
                    SiglaExchange = table.Column<string>(nullable: true),
                    TaxaDeposito = table.Column<decimal>(nullable: false),
                    TaxaMaker = table.Column<decimal>(nullable: false),
                    TaxaSaque = table.Column<decimal>(nullable: false),
                    TaxaTaker = table.Column<decimal>(nullable: false),
                    TickerJsonPath = table.Column<string>(nullable: true),
                    TickerResourcePath = table.Column<string>(nullable: true),
                    UltimoIdOrdemLocal = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Exchanges", x => x.ExchangeId);
                });

            migrationBuilder.CreateTable(
                name: "ParametrosAplicacao",
                columns: table => new
                {
                    ParametrosAplicacaoId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AlertaPercentLucro = table.Column<decimal>(nullable: false),
                    AlertaVolume = table.Column<decimal>(nullable: false),
                    EmitirSomAlerta = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ParametrosAplicacao", x => x.ParametrosAplicacaoId);
                });

            migrationBuilder.CreateTable(
                name: "Trades",
                columns: table => new
                {
                    TradeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trades", x => x.TradeId);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                columns: table => new
                {
                    UsuarioID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Login = table.Column<string>(nullable: true),
                    PrimeiroAcesso = table.Column<bool>(nullable: false),
                    Senha = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.UsuarioID);
                });

            migrationBuilder.CreateTable(
                name: "Ordem",
                columns: table => new
                {
                    OrdemId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AcaoOrdem = table.Column<int>(nullable: false),
                    Asset = table.Column<string>(nullable: true),
                    ExchangeId = table.Column<int>(nullable: true),
                    ExchangeOrdemId = table.Column<string>(nullable: true),
                    Preco = table.Column<decimal>(nullable: false),
                    Quantidade = table.Column<decimal>(type: "decimal(18, 10)", nullable: false),
                    TipoOrdem = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ordem", x => x.OrdemId);
                    table.ForeignKey(
                        name: "FK_Ordem_Exchanges_ExchangeId",
                        column: x => x.ExchangeId,
                        principalTable: "Exchanges",
                        principalColumn: "ExchangeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HistoricoOrdem",
                columns: table => new
                {
                    HistoricoOrdemId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DataHistorico = table.Column<DateTime>(nullable: false),
                    Mensagem = table.Column<string>(nullable: true),
                    OrdemId = table.Column<int>(nullable: false),
                    StatusOrdem = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistoricoOrdem", x => x.HistoricoOrdemId);
                    table.ForeignKey(
                        name: "FK_HistoricoOrdem_Ordem_OrdemId",
                        column: x => x.OrdemId,
                        principalTable: "Ordem",
                        principalColumn: "OrdemId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HistoricoOrdem_OrdemId",
                table: "HistoricoOrdem",
                column: "OrdemId");

            migrationBuilder.CreateIndex(
                name: "IX_Ordem_ExchangeId",
                table: "Ordem",
                column: "ExchangeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistoricoOrdem");

            migrationBuilder.DropTable(
                name: "ParametrosAplicacao");

            migrationBuilder.DropTable(
                name: "Trades");

            migrationBuilder.DropTable(
                name: "Usuario");

            migrationBuilder.DropTable(
                name: "Ordem");

            migrationBuilder.DropTable(
                name: "Exchanges");
        }
    }
}
