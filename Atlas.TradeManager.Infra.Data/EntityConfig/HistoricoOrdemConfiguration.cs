﻿using Atlas.TradeManager.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Atlas.TradeManager.Infra.Data.EntityConfig
{
    public class HistoricoOrdemConfiguration : IEntityTypeConfiguration<HistoricoOrdem>
    {
        public void Configure(EntityTypeBuilder<HistoricoOrdem> builder)
        {
            builder.HasKey(o => o.HistoricoOrdemId);
        }
    }
}
