﻿using Atlas.TradeManager.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Atlas.TradeManager.Infra.Data.EntityConfig
{
    public class OrdemConfiguration : IEntityTypeConfiguration<Ordem>
    {
        public void Configure(EntityTypeBuilder<Ordem> builder)
        {
            builder.HasKey(o => o.OrdemId);
            builder.Property(o => o.Quantidade).HasColumnType("decimal(18, 10)");
        }
    }
}
