﻿using Atlas.TradeManager.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Atlas.TradeManager.Infra.Data.EntityConfig
{
    public class ParametrosAplicacaoConfiguration : IEntityTypeConfiguration<ParametrosAplicacao>
    {
        public void Configure(EntityTypeBuilder<ParametrosAplicacao> builder)
        {
            builder.HasKey(c => c.ParametrosAplicacaoId);
        }
    }
}
