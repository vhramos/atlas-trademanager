﻿using Atlas.TradeManager.Domain.Entities.Exchanges;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Atlas.TradeManager.Infra.Data.EntityConfig
{
    public class ExchangeConfiguration : IEntityTypeConfiguration<Exchange>
    {
        public void Configure(EntityTypeBuilder<Exchange> builder)
        {
            builder.HasKey(c => c.ExchangeId);
        }
    }
}
