﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.CrossCutting.Crypto;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Interfaces.Repositories;

namespace Atlas.TradeManager.Infra.Data.Repositories
{
    public class UsuarioRepository : RepositoryBase<Usuario>, IUsuarioRepository
    {
        public override void Add(Usuario obj)
        {
            obj.Senha = CryptoUtil.HashPassword(obj.Senha);
            base.Add(obj);
        }
        public override void Update(Usuario obj)
        {
            obj.Senha = CryptoUtil.HashPassword(obj.Senha);
            base.Update(obj);
        }
    }
}
