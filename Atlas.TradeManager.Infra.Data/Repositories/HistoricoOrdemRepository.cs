﻿using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Interfaces.Repositories;

namespace Atlas.TradeManager.Infra.Data.Repositories
{
    public class HistoricoOrdemRepository : RepositoryBase<HistoricoOrdem>, IHistoricoOrdemRepository
    {
    }
}
