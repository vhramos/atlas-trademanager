﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Interfaces.Repositories;

namespace Atlas.TradeManager.Infra.Data.Repositories
{
    public class TradeRepository : RepositoryBase<Trade>, ITradeRepository
    {
    }
}
