﻿using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Atlas.TradeManager.Infra.Data.Repositories
{
    public class OrdemRepository : RepositoryBase<Ordem>, IOrdemRepository
    {
        public override IEnumerable<Ordem> GetAll()
        {
            return from ordens in Db.Ordem
                   join exchange in Db.Exchanges on ordens.Exchange.ExchangeId equals exchange.ExchangeId
                   select ordens;
        }
        public override void Update(Ordem obj)
        {
            //TODO: VALIDAR NECESSIDADE
            foreach (var historico in obj.Historico)
            {
                if (historico.HistoricoOrdemId == 0)
                    Db.Entry(historico).State = EntityState.Added;
            }
            base.Update(obj);
        }
    }
}
