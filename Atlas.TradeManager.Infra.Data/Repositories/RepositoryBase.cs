﻿using Atlas.TradeManager.Domain.Entities.Exchanges;
using Atlas.TradeManager.Domain.Interfaces.Repositories;
using Atlas.TradeManager.Infra.Data.Contexto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Atlas.TradeManager.Infra.Data.Repositories
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected TradeManagerContext Db = new TradeManagerContext();
        public virtual void Add(TEntity obj)
        {
            Db.Set<TEntity>().Add(obj);
            //TODO: PEGAR PELA CLASSE BASE EXCHANGE
            foreach (var entry in Db.ChangeTracker.Entries().Where(entry => entry.Entity.GetType() == typeof(BitcoinToYou) ||
                                                                entry.Entity.GetType() == typeof(BitcoinTrade) ||
                                                                entry.Entity.GetType() == typeof(Braziliex) ||
                                                                entry.Entity.GetType() == typeof(BlinkTrade) ||
                                                                entry.Entity.GetType() == typeof(MercadoBitcoin)))

            {
                if (((Exchange)entry.Entity).ExchangeId > 0)
                    entry.State = EntityState.Unchanged;
            }
            Db.SaveChanges();
        }
        public TEntity GetById(int id)
        {
            return Db.Set<TEntity>().Find(id);
        }
        public virtual IEnumerable<TEntity> GetAll()
        {
            return Db.Set<TEntity>().ToList();
        }
        public virtual void Update(TEntity obj)
        {
            Db.Entry(obj).State = EntityState.Modified;
            Db.SaveChanges();
        }
        public void Remove(TEntity obj)
        {
            Db.Set<TEntity>().Remove(obj);
            Db.SaveChanges();
        }
        public void Dispose()
        {
            Db.Dispose();
        }
    }
}
