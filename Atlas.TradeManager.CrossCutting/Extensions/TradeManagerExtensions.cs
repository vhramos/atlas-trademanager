﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;
using System.Threading;
using System.Data;

namespace Atlas.TradeManager.CrossCutting.Extensions
{
    public class SortData
    {
        public int PropertyIndex { get; set; } = -1;
        public bool Ascending { get; set; } = true;
    }
    public static class TradeManagerExtensions
    {
        private static CultureInfo defaultCultureInfo = CultureInfo.CurrentCulture;
        private static CultureInfo _cultureInfoBrasileira = CultureInfo.GetCultureInfo("pt-BR");
        private const string BITCOIN_CURRENCY_SYMBOL = "Ƀ";
        private static readonly string BRL_CURRENCY_SYMBOL = _cultureInfoBrasileira.NumberFormat.CurrencySymbol;
        private static readonly string PERCENT_SYMBOL = _cultureInfoBrasileira.NumberFormat.PercentSymbol;
        public static T Converter<T>(this object obj)
        {
            return (T)Convert.ChangeType(obj, typeof(T), defaultCultureInfo);
        }
        public static string ToDescription(this Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(
                                              typeof(DescriptionAttribute),

                                              false);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }
            return en.ToString();
        }
        public static T ToEnum<T>(this string enumString)
        {
            return (T)Enum.Parse(typeof(T), enumString, true);
        }
        public static T GetValueFromDescription<T>(this object description)
        {
            string strDescription = description.ToString();
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == strDescription)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == strDescription)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "description");
        }
        public static DataTable ToDataTable<T>(this List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties by using reflection   
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names  
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }

            return dataTable;
        }
        public static IEnumerable<T> OrderByIndex<T>(this IEnumerable<T> lista, SortData sort)
        {
            if (sort != null && sort.PropertyIndex > -1)
            {
                var browsableProperties = lista.GetType().GetGenericArguments().Single().GetProperties().ToList().FindAll(p => !Attribute.IsDefined(p, typeof(BrowsableAttribute))
                                                                                                            || ((BrowsableAttribute)p.GetCustomAttribute(typeof(BrowsableAttribute), false)).Browsable);
                PropertyInfo property = browsableProperties[sort.PropertyIndex];
                if (sort.Ascending)
                    lista = lista.OrderBy(l => property.FormatedValue(l));
                else
                    lista = lista.OrderByDescending(l => property.FormatedValue(l));
            }
            return lista.ToList();
        }
        private static object FormatedValue(this PropertyInfo property, object obj)
        {
            object value = property.GetValue(obj, null).ToString().Replace(BRL_CURRENCY_SYMBOL, string.Empty).Replace(BITCOIN_CURRENCY_SYMBOL, string.Empty).Replace(PERCENT_SYMBOL, string.Empty);
            if (decimal.TryParse(value.ToString(), NumberStyles.Any, _cultureInfoBrasileira, out decimal parse))
                return parse;
            else
                return value;
        }
        public static string ToRealFormat(this decimal valor)
        {
            return string.Format(_cultureInfoBrasileira, "{0:C}", valor);
        }
        public static string ToBitcoinFormat(this decimal valor)
        {
            var numberFormatInfo = (NumberFormatInfo)_cultureInfoBrasileira.NumberFormat.Clone();
            numberFormatInfo.CurrencySymbol = BITCOIN_CURRENCY_SYMBOL;
            return string.Format(numberFormatInfo, "{0:C8}", valor);
        }
        public static string ToPercentFormat(this decimal valor)
        {
            return string.Format(_cultureInfoBrasileira, string.Concat("{0:N}", PERCENT_SYMBOL), valor);
        }
    }
}