﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Application.Interface.AppService;
using Atlas.TradeManager.Application.Singleton;
using Atlas.TradeManager.CrossCutting.Extensions;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Interfaces.Services;

namespace Atlas.TradeManager.Application.AppService
{
    public class CotacaoMoedaAppService : AppServiceBase<CotacaoMoeda>, ICotacaoMoedaAppService
    {
        public CotacaoMoedaAppService() : base(null)
        {
            if (TradeManagerSingleton.CotacaoBitcoin == null)
                TradeManagerSingleton.CotacaoBitcoin = new CotacaoMoeda();

            if (TradeManagerSingleton.CotacaoDolar == null)
                TradeManagerSingleton.CotacaoDolar = new CotacaoMoeda();
        }
        public CotacaoMoeda ConsultarCotacaoBitcoin()
        {
            return TradeManagerSingleton.CotacaoBitcoin;
        }
        public CotacaoMoeda ConsultarCotacaoDolar()
        {
            return TradeManagerSingleton.CotacaoDolar;
        }
    }
}
