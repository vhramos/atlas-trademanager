﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using Atlas.TradeManager.Application.Interface;
using Atlas.TradeManager.Application.Interface.AppService;
using Atlas.TradeManager.Application.Interface.Task;
using Atlas.TradeManager.Application.Singleton;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Entities.Exchanges;
using Atlas.TradeManager.Domain.Interfaces.Services;
using Atlas.TradeManager.Services.Interface;

namespace Atlas.TradeManager.Application.AppService
{
    public class ExchangeAppService : AppServiceBase<Exchange>, IExchangeAppService
    {
        private readonly IExchangeService _exchangeService;
        private readonly ICotacaoMoedaAppService _cotacaoMoedaAppService;
        private readonly IExternalService _externalService;
        public ExchangeAppService(IExchangeService exchangeService,
                                  ICotacaoMoedaAppService cotacaoMoedaAppService,
                                  IExternalService externalService)
            : base(exchangeService)
        {
            _cotacaoMoedaAppService = cotacaoMoedaAppService;
            _exchangeService = exchangeService;
            _externalService = externalService;
            if (TradeManagerSingleton.Exchanges == null)
                TradeManagerSingleton.Exchanges = GetAll().ToList();
            AtualizarSaldos();
        }
        public decimal CalcularValorAgio(Exchange exchange)
        {
            decimal montante = _cotacaoMoedaAppService.ConsultarCotacaoBitcoin().Valor * _cotacaoMoedaAppService.ConsultarCotacaoDolar().Valor;
            if (montante > 0)
                return Math.Round(((exchange.PrecoAsk / montante) - 1) * 100, 2);
            return montante;
        }
        public void ConfigurarOpcoesDeTaxas(bool aplicarTaxaDeposito, bool aplicarTaxaSaque, bool executarOrdemMaker)
        {
            foreach (var exchange in TradeManagerSingleton.Exchanges)
            {
                exchange.AplicarTaxaDeposito = aplicarTaxaDeposito;
                exchange.AplicarTaxaSaque = aplicarTaxaSaque;
                exchange.ExecutarOrdensMaker = executarOrdemMaker;
            }
        }
        public List<Exchange> ConsultarExchangesCarregadas()
        {
            return TradeManagerSingleton.Exchanges.FindAll(e => e.DadosCarregados);
        }
        public List<Exchange> ConsultarExchangesCarregadasOnline()
        {
            return ConsultarExchangesCarregadas().FindAll(e => e.ExchangeOnline);
        }
        public Exchange ConsultarPorNome(string nome)
        {
            return _exchangeService.GetAll().FirstOrDefault(e => e.NomeExchange == nome);
        }
        public async Task AtualizarSaldos()
        {
            TradeManagerSingleton.Exchanges.FindAll(e => e.SiglaExchange != "NEG").ForEach(e => AtualizarSaldos(e));
        }
        public async Task AtualizarSaldos(Exchange exchange)
        {
            await Task.Run(() =>
            {
                if (_externalService.EfetuarRequisicaoHttp(exchange.CriarRequesicaoConsultaSaldo(), out string respostaHttp))
                    exchange.AtualizarSaldo(respostaHttp);
            });
        }
        public void ForcarValoresSaldo(Exchange exchange)
        {
            Exchange exchangeSel = TradeManagerSingleton.Exchanges.First(e => e.ExchangeId == exchange.ExchangeId);
            exchangeSel.SaldoBRL = exchange.SaldoBRL;
            exchangeSel.SaldoBTC = exchange.SaldoBTC;
        }
    }
}
