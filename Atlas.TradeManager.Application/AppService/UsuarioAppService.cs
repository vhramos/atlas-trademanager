﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Application.Interface.AppService;
using Atlas.TradeManager.Application.Singleton;
using Atlas.TradeManager.CrossCutting.Crypto;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Interfaces.Services;

namespace Atlas.TradeManager.Application.AppService
{
    public class UsuarioAppService : AppServiceBase<Usuario>, IUsuarioAppService
    {
        private readonly IUsuarioService _usuarioService;
        public UsuarioAppService(IUsuarioService usuarioService) : base(usuarioService)
        {
            _usuarioService = usuarioService;
        }
        public Usuario EfetuarLogin(Usuario usuario)
        {
            return _usuarioService.GetAll().FirstOrDefault(u => u.Login == usuario.Login && u.Senha == CryptoUtil.HashPassword(usuario.Senha));
        }
    }
}
