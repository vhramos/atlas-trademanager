﻿
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Application.Interface.AppService;
using Atlas.TradeManager.Application.Singleton;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Entities.Exchanges;
using Atlas.TradeManager.Domain.Interfaces.Services;
using Atlas.TradeManager.Services.Interface;

namespace Atlas.TradeManager.Application.AppService
{
    public class TradeAppService : AppServiceBase<Trade>, ITradeAppService
    {
        private readonly IExchangeAppService _exchangeAppService;
        private readonly IParametroAplicacaoAppService _parametroAplicacaoAppService;
        private readonly IOrdemAppService _ordemAppService;
        public TradeAppService(ITradeService tradeService,
                               IExchangeAppService exchangeAppService,
                               IParametroAplicacaoAppService parametroAplicacaoAppService,
                               IOrdemAppService ordemAppService)
            : base(tradeService)
        {
            _exchangeAppService = exchangeAppService;
            _parametroAplicacaoAppService = parametroAplicacaoAppService;
            _ordemAppService = ordemAppService;
        }
        public bool AlertarMelhorTrade(Trade trade)
        {
            ParametrosAplicacao parametros = _parametroAplicacaoAppService.ConsultarParametros();
            bool alertar = parametros != null &&
                           (TradeManagerSingleton.UltimaTradeAlertada == null ||
                           trade.PercentualLucroLiquido != TradeManagerSingleton.UltimaTradeAlertada.PercentualLucroLiquido ||
                           trade.QtdCompraBTC != TradeManagerSingleton.UltimaTradeAlertada.QtdCompraBTC) &&
                           trade.PercentualLucroLiquido >= parametros.AlertaPercentLucro &&
                           trade.QtdCompraBTC >= parametros.AlertaVolume;
            if (alertar)
                TradeManagerSingleton.UltimaTradeAlertada = trade;
            return alertar;
        }
        public IEnumerable<Trade> ConsultarPossiveisTrades()
        {
            var exchangesTmp = _exchangeAppService.ConsultarExchangesCarregadasOnline();
            List<Trade> lstTrades = new List<Trade>();
            foreach (var exchange1 in _exchangeAppService.ConsultarExchangesCarregadasOnline())
            {
                foreach (var exchange2 in exchangesTmp)
                {
                    if (exchange1.ExchangeId != exchange2.ExchangeId)
                    {
                        Trade trade = new Trade(exchange1, exchange2);
                        lstTrades.Add(trade);
                    }
                }
                exchangesTmp.RemoveAll(e => e.ExchangeId == exchange1.ExchangeId);
            }
            return lstTrades.OrderByDescending(t => t.LucroLiquido);
        }
        public async Task ExecutarTrade(Trade trade)
        {
            await Task.Run(() =>
            {
                Ordem ordemCompra = new Ordem()
                {
                    Exchange = trade.ExchangeAsk,
                    AcaoOrdem = AcaoOrdem.Compra,
                    Asset = "BTC",
                    Preco = trade.ExchangeAsk.PrecoAsk,
                    Quantidade = trade.QtdCompraBTC,
                    TipoOrdem = TipoOrdem.Limit
                };

                Ordem ordemVenda = new Ordem()
                {
                    Exchange = trade.ExchangeBid,
                    AcaoOrdem = AcaoOrdem.Venda,
                    Asset = "BTC",
                    Preco = trade.ExchangeBid.PrecoBid,
                    Quantidade = trade.QtdVendaBTC,
                    TipoOrdem = TipoOrdem.Limit
                };

                if (!ExchangeEhNegocieCoins(ordemCompra.Exchange))
                    _ordemAppService.CriarOrdem(ordemCompra);
                if (!ExchangeEhNegocieCoins(ordemVenda.Exchange))
                    _ordemAppService.CriarOrdem(ordemVenda);
                _exchangeAppService.AtualizarSaldos(ordemCompra.Exchange);
                _exchangeAppService.AtualizarSaldos(ordemVenda.Exchange);
            });
        }
        private bool ExchangeEhNegocieCoins(Exchange exchange)
        {
            return exchange.SiglaExchange == "NEG";
        }
    }
}
