﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using Atlas.TradeManager.Application.Interface;
using Atlas.TradeManager.Application.Interface.AppService;
using Atlas.TradeManager.Application.Interface.Task;
using Atlas.TradeManager.Application.Singleton;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Entities.Exchanges;
using Atlas.TradeManager.Domain.Interfaces.Services;
using Atlas.TradeManager.Services.Interface;

namespace Atlas.TradeManager.Application.AppService
{
    public class HistoricoOrdemAppService : AppServiceBase<HistoricoOrdem>, IHistoricoOrdemAppService
    {
        public HistoricoOrdemAppService(IHistoricoOrdemService historicoOrdemService)
            : base(historicoOrdemService)
        {
        }
    }
}
