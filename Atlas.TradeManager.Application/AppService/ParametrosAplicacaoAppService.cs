﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Application.Interface.AppService;
using Atlas.TradeManager.Application.Singleton;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Interfaces.Services;

namespace Atlas.TradeManager.Application.AppService
{
    public class ParametrosAplicacaoAppService : AppServiceBase<ParametrosAplicacao>, IParametroAplicacaoAppService
    {
        private readonly IParametrosAplicacaoService _parametrosAplicacaoService;
        public ParametrosAplicacaoAppService(IParametrosAplicacaoService parametrosAplicacaoService) : base(parametrosAplicacaoService)
        {
            _parametrosAplicacaoService = parametrosAplicacaoService;
        }
        public void AlterarParametros(ParametrosAplicacao parametros)
        {
            if (ConsultarParametros() == null)
                _parametrosAplicacaoService.Add(parametros);
            else
                _parametrosAplicacaoService.Update(parametros);
            TradeManagerSingleton.ParametrosAplicacao = parametros;
        }
        public ParametrosAplicacao ConsultarParametros()
        {
            if (TradeManagerSingleton.ParametrosAplicacao == null)
                TradeManagerSingleton.ParametrosAplicacao = _parametrosAplicacaoService.GetAll().FirstOrDefault();
            return TradeManagerSingleton.ParametrosAplicacao;
        }
    }
}
