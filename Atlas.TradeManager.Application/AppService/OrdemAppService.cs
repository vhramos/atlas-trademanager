﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using Atlas.TradeManager.Application.Interface;
using Atlas.TradeManager.Application.Interface.AppService;
using Atlas.TradeManager.Application.Interface.Task;
using Atlas.TradeManager.Application.Singleton;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Entities.Exchanges;
using Atlas.TradeManager.Domain.Interfaces.Services;
using Atlas.TradeManager.Services.Interface;

namespace Atlas.TradeManager.Application.AppService
{
    public class OrdemAppService : AppServiceBase<Ordem>, IOrdemAppService
    {
        private readonly IOrdemService _ordemService;
        private readonly IExternalService _externalService;
        private readonly IHistoricoOrdemAppService _historicoOrdemAppService;
        private readonly IExchangeAppService _exchangeAppService;
        public OrdemAppService(IOrdemService ordemService,
                               IHistoricoOrdemAppService historicoOrdemAppService,
                               IExternalService externalService,
                               IExchangeAppService exchangeAppService)
            : base(ordemService)
        {
            _ordemService = ordemService;
            _externalService = externalService;
            _historicoOrdemAppService = historicoOrdemAppService;
            _exchangeAppService = exchangeAppService;
        }

        public IEnumerable<Ordem> ConsultarOrdens()
        {
            List<Ordem> lst = GetAll().ToList();
            foreach (var ordem in lst)
            {
                //TODO: MELHORAR ISSO
                ordem.Historico = _historicoOrdemAppService.GetAll().Where(o => o.OrdemId == ordem.OrdemId).ToList();
            }
            return lst;
        }
        public IEnumerable<Ordem> ConsultarOrdensAtivas()
        {
            //TODO: ANALISAR CASOS DE PARTIAL FILLED
            return GetAll().ToList().FindAll(o => o.StatusAtual != StatusOrdem.Rejeitada
                                               && o.StatusAtual != StatusOrdem.Consumida 
                                               && o.StatusAtual != StatusOrdem.Cancelada);
        }
        public void CriarOrdem(Ordem ordem)
        {
            if (ordem.Exchange.UltimoIdOrdemLocal.HasValue)
            {
                ordem.Exchange.UltimoIdOrdemLocal += 1;
                ordem.ExchangeOrdemId = ordem.Exchange.UltimoIdOrdemLocal.ToString();
                _exchangeAppService.Update(ordem.Exchange);
            }
            RequisicaoHttp requestCriacao = ordem.Exchange.CriarRequesicaoNovaOrdem(ordem);
            _externalService.EfetuarRequisicaoHttp(requestCriacao, out string respostaHttp);
            GravarHistoricoOrdem(ordem, respostaHttp);
        }
        public void GerarHistoricoOrdem(Ordem ordem)
        {
            RequisicaoHttp requestHistorico = ordem.Exchange.CriarRequesicaoConsultaHistoricoOrdem(ordem);
            _externalService.EfetuarRequisicaoHttp(requestHistorico, out string respostaHttp);
            GravarHistoricoOrdem(ordem, respostaHttp);
        }
        public void GravarHistoricoOrdem(Ordem ordem, string respostaHttp)
        {
            ordem.Exchange.PreencherOrdem(ref ordem, respostaHttp);
            SalvarOrdem(ordem);
        }
        private void SalvarOrdem(Ordem ordem)
        {
            if (ordem.OrdemId <= 0)
                _ordemService.Add(ordem);
            else
            {
                if (ordem.StatusAtual != ordem.Historico.Last().StatusOrdem)
                    _ordemService.Update(ordem);
            }
        }
    }
}
