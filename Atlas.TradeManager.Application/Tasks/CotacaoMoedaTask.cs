﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Atlas.TradeManager.Application.Interface.Task;
using Atlas.TradeManager.Application.Singleton;
using Atlas.TradeManager.CrossCutting.Extensions;
using Atlas.TradeManager.Domain.Entities;

namespace Atlas.TradeManager.Application.Tasks
{
    public class CotacaoMoedaTask : TaskBase<CotacaoMoeda>, ICotacaoMoedaTask
    {
        #region Constantes
        private const string GATEWAY_COTACAO_BTC = "https://api.bitfinex.com/";
        private const string RESOURCE_COTACAO_BTC = "v2/ticker/tBTCUSD";
        private const string GATEWAY_COTACAO_DOLAR = "http://api.promasters.net.br/cotacao/";
        private const string RESOURCE_COTACAO_DOLAR = "v1/valores?moedas=USD&alt=json";

        public CotacaoMoedaTask() : base(300000)
        {
        }
        #endregion

        #region Metodos
        public async override void Executar()
        {
            await AtualizarCotacaoBitcoin();
            await AtualizarCotacaoDolar();
        }
        private async Task AtualizarCotacaoBitcoin()
        {
            await Task.Run(() =>
            {
                CotacaoMoeda cotacaoMoeda = null;
                decimal cotacao = 0;
                var cotacaoJson = HttpUtil.RetornarResponseJson(GATEWAY_COTACAO_BTC, RESOURCE_COTACAO_BTC);
                if (cotacaoJson != null && decimal.TryParse(cotacaoJson[0].ToString(), out cotacao))
                {
                    cotacaoMoeda = new CotacaoMoeda()
                    {
                        Valor = cotacao,
                        DataCotacaoUTC = DateTime.UtcNow
                    };
                    TradeManagerSingleton.CotacaoBitcoin = cotacaoMoeda;
                }
            });
        }
        private async Task AtualizarCotacaoDolar()
        {
            await Task.Run(() =>
            {
                CotacaoMoeda cotacaoMoeda = null;
                decimal cotacao = 0;
                var cotacaoJson = HttpUtil.RetornarResponseJson(GATEWAY_COTACAO_DOLAR, RESOURCE_COTACAO_DOLAR);
                if (cotacaoJson != null && decimal.TryParse(cotacaoJson["valores"]["USD"]["valor"].ToString(), out cotacao))
                {
                    cotacaoMoeda = new CotacaoMoeda()
                    {
                        Valor = cotacao,
                        DataCotacaoUTC = DateTime.UtcNow
                    };
                    TradeManagerSingleton.CotacaoDolar = cotacaoMoeda;
                }
                return cotacaoMoeda;
            });
        }
        #endregion
    }
}
