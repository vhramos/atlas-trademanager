﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Application.Interface.AppService;
using Atlas.TradeManager.Application.Interface.Task;
using Atlas.TradeManager.Application.Singleton;
using Atlas.TradeManager.CrossCutting.Extensions;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Entities.Exchanges;
using Atlas.TradeManager.Domain.Interfaces.Services;
using Atlas.TradeManager.Services.Interface;

namespace Atlas.TradeManager.Application.Tasks
{
    public class OrdemTask : TaskBase<Ordem>, IOrdemTask
    {
        private readonly IOrdemAppService _ordemAppService;
        public OrdemTask(IOrdemAppService ordemAppService)
            : base(1200)
        {
            _ordemAppService = ordemAppService;
        }


        public override void Executar()
        {
            //TODO: REMOVER POOLING(getall -ver profiller) NO BANCO E ARMAZENAR EM SINGLETON
            try
            {
                var teste = _ordemAppService.ConsultarOrdensAtivas().ToList();
                foreach (var ordemAtiva in teste)
                {
                    _ordemAppService.GerarHistoricoOrdem(ordemAtiva);
                }
            }
            catch (Exception erro)
            {

            }
        }
    }
}
