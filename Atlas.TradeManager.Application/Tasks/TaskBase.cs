﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Atlas.TradeManager.Application.Interface.Task;

namespace Atlas.TradeManager.Application.Tasks
{
    public class TaskBase<TEntity> : ITaskBase<TEntity> where TEntity : class
    {
        private readonly Timer _timerTask;
        public TaskBase(int intervalo)
        {
            _timerTask = new Timer(intervalo);
            _timerTask.AutoReset = true;
            _timerTask.Elapsed += TmrExecutar_Elapsed;
            _timerTask.Start();
        }
        private void TmrExecutar_Elapsed(object sender, ElapsedEventArgs e)
        {
            _timerTask.Stop();
            Executar();
            _timerTask.Start();
        }
        public virtual void Executar()
        {
            throw new NotImplementedException();
        }
    }
}
