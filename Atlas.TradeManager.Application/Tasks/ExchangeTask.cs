﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Application.Interface.AppService;
using Atlas.TradeManager.Application.Interface.Task;
using Atlas.TradeManager.Application.Singleton;
using Atlas.TradeManager.CrossCutting.Extensions;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Entities.Exchanges;
using Atlas.TradeManager.Domain.Interfaces.Services;
using Atlas.TradeManager.Services.Interface;

namespace Atlas.TradeManager.Application.Tasks
{
    public class ExchangeTask : TaskBase<Exchange>, IExchangeTask
    {
        public ExchangeTask(IExternalService externalService)
            : base(500)
        {
        }
        public async override void Executar()
        {
            if (TradeManagerSingleton.Exchanges != null)
            {
                foreach (var exchange in TradeManagerSingleton.Exchanges)
                {
                    var watch = System.Diagnostics.Stopwatch.StartNew();
                    await AtualizarOrderBook(exchange);
                    await AtualizarTicker(exchange);
                    exchange.Ping = watch.ElapsedMilliseconds;
                }
            }
        }
        private async Task AtualizarOrderBook(Exchange exchange)
        {
            await Task.Run(() =>
            {
                OrderBook orderBook = new OrderBook();
                var orderBookJson = HttpUtil.RetornarResponseJson(exchange.GatewayUrl, exchange.OrderBookResourcePath);
                if (orderBookJson != null)
                {
                    if (!string.IsNullOrEmpty(exchange.OrderBookJsonPath))
                    {
                        foreach (string tag in exchange.OrderBookJsonPath.Split(';'))
                        {
                            orderBookJson = orderBookJson[tag];
                        }
                    }
                    object priceKey = exchange.JsonOrderBookPricePath;
                    object amountKey = exchange.JsonOrderBookAmountPath;

                    if (int.TryParse(priceKey.ToString(), out int priceIndex))
                        priceKey = priceIndex;
                    if (int.TryParse(amountKey.ToString(), out int amountIndex))
                        amountKey = amountIndex;

                    foreach (var bid in orderBookJson[exchange.JsonBidName])
                    {
                        orderBook.Bids.Add(new Oferta()
                        {
                            //TODO: REMOVER CONVERT
                            Price = Convert.ToDecimal(bid[priceKey]),
                            Amount = Convert.ToDecimal(bid[amountKey])
                        });
                    }
                    foreach (var ask in orderBookJson[exchange.JsonAskName])
                    {
                        orderBook.Asks.Add(new Oferta()
                        {
                            Price = Convert.ToDecimal(ask[priceKey]),
                            Amount = Convert.ToDecimal(ask[amountKey])
                        });
                    }
                    exchange.OrderBook = orderBook;
                }
            });
        }
        private async Task AtualizarTicker(Exchange exchange)
        {
            await Task.Run(() =>
            {
                Ticker ticker = new Ticker();
                var tickerJson = HttpUtil.RetornarResponseJson(exchange.GatewayUrl, exchange.TickerResourcePath);
                if (tickerJson != null)
                {
                    if (!string.IsNullOrEmpty(exchange.TickerJsonPath))
                    {
                        foreach (string tag in exchange.TickerJsonPath.Split(';'))
                        {
                            tickerJson = tickerJson[tag];
                        }
                    }
                    object lastKey = exchange.JsonLastName;
                    object volumnKey = exchange.JsonVolumnName;
                    if (int.TryParse(lastKey.ToString(), out int lastIndex))
                        lastKey = lastIndex;
                    if (int.TryParse(volumnKey.ToString(), out int volumnIndex))
                        volumnKey = volumnIndex;
                    ticker.Last = Convert.ToDecimal(tickerJson[lastKey]);
                    ticker.Volumn = Convert.ToDecimal(tickerJson[volumnKey]);
                    exchange.Ticker = ticker;
                }
            });
        }
    }
}
