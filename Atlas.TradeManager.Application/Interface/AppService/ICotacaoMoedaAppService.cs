﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Entities;

namespace Atlas.TradeManager.Application.Interface.AppService
{
    public interface ICotacaoMoedaAppService : IAppServiceBase<CotacaoMoeda>
    {
        CotacaoMoeda ConsultarCotacaoDolar();
        CotacaoMoeda ConsultarCotacaoBitcoin();
    }
}
