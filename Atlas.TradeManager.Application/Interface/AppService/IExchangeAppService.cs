﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Entities.Exchanges;

namespace Atlas.TradeManager.Application.Interface.AppService
{
    public interface IExchangeAppService : IAppServiceBase<Exchange>
    {
        decimal CalcularValorAgio(Exchange exchange);
        List<Exchange> ConsultarExchangesCarregadas();
        List<Exchange> ConsultarExchangesCarregadasOnline();
        Exchange ConsultarPorNome(string nome);
        void ConfigurarOpcoesDeTaxas(bool aplicarTaxaDeposito, bool aplicarTaxaSaque, bool executarOrdemMaker);
        System.Threading.Tasks.Task AtualizarSaldos(Exchange exchange);
        System.Threading.Tasks.Task AtualizarSaldos();
        void ForcarValoresSaldo(Exchange exchange);
    }
}