﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Entities;

namespace Atlas.TradeManager.Application.Interface.AppService
{
    public interface ITradeAppService : IAppServiceBase<Trade>
    {
        IEnumerable<Trade> ConsultarPossiveisTrades();
        bool AlertarMelhorTrade(Trade trade);
        System.Threading.Tasks.Task ExecutarTrade(Trade trade);
    }
}
