﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Entities;

namespace Atlas.TradeManager.Application.Interface.Task
{
    public interface ICotacaoMoedaTask : ITaskBase<CotacaoMoeda>
    {
    }
}
