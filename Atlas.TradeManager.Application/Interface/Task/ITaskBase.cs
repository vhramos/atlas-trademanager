﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Application.Interface.Task
{
    public interface ITaskBase<TEntity> where TEntity : class
    {
        void Executar();
    }
}
