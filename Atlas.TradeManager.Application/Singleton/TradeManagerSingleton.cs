﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Application.Interface.AppService;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Entities.Exchanges;

namespace Atlas.TradeManager.Application.Singleton
{
    public static class TradeManagerSingleton
    {
        #region Propriedades
        public static List<Exchange> Exchanges { get; set; }
        public static CotacaoMoeda CotacaoBitcoin { get; set; }
        public static CotacaoMoeda CotacaoDolar { get; set; }
        public static Trade UltimaTradeAlertada { get; set; }
        public static ParametrosAplicacao ParametrosAplicacao { get; set; }
        #endregion
    }
}
