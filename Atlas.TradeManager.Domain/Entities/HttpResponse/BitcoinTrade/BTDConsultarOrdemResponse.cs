﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BitcoinTrade
{
    public class BtdConsultarOrdemResponse
    {
        [JsonProperty(PropertyName = "message")]
        public string Mensagem { get; set; }
        [JsonProperty(PropertyName = "data")]
        private BtdConsultarOrdemResponseItem OrdemItem { get; set; }

        public BtdOrdemResponse Ordem { get; set; }

        public BtdConsultarOrdemResponse()
        {
        }
        public BtdConsultarOrdemResponse(string respostaHttp, string codigoOrdem)
        {
            var orderResponse = JsonConvert.DeserializeObject<BtdConsultarOrdemResponse>(respostaHttp);
            Mensagem = orderResponse.Mensagem;
            OrdemItem = orderResponse.OrdemItem;
            if (OrdemItem != null && OrdemItem.Ordens != null)
                Ordem = OrdemItem.Ordens.Find(o => o.Codigo == codigoOrdem);
        }
    }
}
