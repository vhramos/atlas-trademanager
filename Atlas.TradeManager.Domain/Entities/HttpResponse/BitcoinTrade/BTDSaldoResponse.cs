﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BitcoinTrade
{
    public class BtdSaldoResponse
    {
        [JsonProperty(PropertyName = "message")]
        public string message { get; set; }
        [JsonProperty(PropertyName = "data")]
        public List<BtdSaldoResponseItem> ItensSaldo { get; set; }

        public BtdSaldoResponse()
        {
        }
        public BtdSaldoResponse(string respostaHttp)
        {
            ItensSaldo = JsonConvert.DeserializeObject<BtdSaldoResponse>(respostaHttp).ItensSaldo;
        }
    }
}
