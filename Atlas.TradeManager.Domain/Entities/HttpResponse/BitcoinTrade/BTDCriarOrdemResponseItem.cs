﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BitcoinTrade
{
    public class BtdCriarOrdemResponseItem
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        [JsonProperty(PropertyName = "code")]
        public string Codigo { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Tipo { get; set; }
        [JsonProperty(PropertyName = "unit_price")]
        public decimal Preco { get; set; }
        [JsonProperty(PropertyName = "user_code")]
        public string CodigoUsuario { get; set; }
        [JsonProperty(PropertyName = "currency_code")]
        public string CodigoMoeda { get; set; }
        [JsonProperty(PropertyName = "amount")]
        public decimal Quantidade { get; set; }
    }
}
