﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BitcoinTrade
{
    public class BtdCriarOrdemResponse
    {
        [JsonProperty(PropertyName = "message")]
        public string Mensagem { get; set; }
        [JsonProperty(PropertyName = "data")]
        public BtdCriarOrdemResponseItem Ordem { get; set; }

        public BtdCriarOrdemResponse()
        {
        }
        public BtdCriarOrdemResponse(string respostaHttp)
        {
            var orderResponse = JsonConvert.DeserializeObject<BtdCriarOrdemResponse>(respostaHttp);
            Mensagem = orderResponse.Mensagem;
            Ordem = orderResponse.Ordem;

        }
    }
}
