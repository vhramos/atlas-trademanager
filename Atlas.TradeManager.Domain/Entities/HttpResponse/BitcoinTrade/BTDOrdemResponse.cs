﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BitcoinTrade
{
    public class BtdOrdemResponse
    {
        [JsonProperty(PropertyName = "code")]
        public string Codigo { get; set; }
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }
    }
}
