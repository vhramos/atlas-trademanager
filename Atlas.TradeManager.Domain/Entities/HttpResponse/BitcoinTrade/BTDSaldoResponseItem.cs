﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BitcoinTrade
{
    public class BtdSaldoResponseItem
    {
        [JsonProperty(PropertyName = "currency_code")]
        public string CodigoMoeda { get; set; }
        [JsonProperty(PropertyName = "available_amount")]
        public decimal Quantidade { get; set; }
    }
}
