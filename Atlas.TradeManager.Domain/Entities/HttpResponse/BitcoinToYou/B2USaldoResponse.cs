﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BitcoinToYou
{
    public class B2USaldoResponse : BaseResponse
    {
        [JsonProperty("oReturn")]
        private List<B2USaldoResponseItem> ItensSaldo { get; set; }
        public B2USaldoResponseItem Saldos
        {
            get
            {
                B2USaldoResponseItem saldo = null;
                if (ItensSaldo != null && ItensSaldo.FirstOrDefault() != null)
                    saldo = ItensSaldo.FirstOrDefault();
                return saldo;
            }
        }
        public B2USaldoResponse()
        {
        }
        public B2USaldoResponse(string respostaHttp)
        {
            ItensSaldo = JsonConvert.DeserializeObject<B2USaldoResponse>(respostaHttp).ItensSaldo;
        }
    }
}
