﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BitcoinToYou
{
    public class B2UOrdemResponse : BaseResponse
    {
        [JsonProperty("oReturn")]
        private List<B2UOrdemResponseItem> ItensOrdem { get; set; }
        public B2UOrdemResponseItem Ordem
        {
            get
            {
                B2UOrdemResponseItem ordem = null;
                if (ItensOrdem != null && ItensOrdem.FirstOrDefault() != null)
                    ordem = ItensOrdem.FirstOrDefault();
                return ordem;
            }
        }
        public B2UOrdemResponse()
        {
        }
        public B2UOrdemResponse(string respostaHttp)
        {
            var ordemResponse = JsonConvert.DeserializeObject<B2UOrdemResponse>(respostaHttp);
            if (ordemResponse != null && ordemResponse.ItensOrdem != null)
                ItensOrdem = ordemResponse.ItensOrdem;
        }
    }
}
