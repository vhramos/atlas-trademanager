﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BitcoinToYou
{
    public class B2USaldoResponseItem
    {
        [JsonProperty(PropertyName = "BRL")]
        public decimal SaldoBRL { get; set; }
        [JsonProperty(PropertyName = "BTC")]
        public decimal SaldoBTC { get; set; }
    }
}
