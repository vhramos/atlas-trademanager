﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.Braziliex
{
    public class BzxConsultarOrdemResponse
    {

        [JsonProperty(PropertyName = "trade_history")]
        private List<BzxConsultarOrdemResponseItem> Ordens { get; set; }
        public BzxConsultarOrdemResponseItem Ordem { get; set; }
        public BzxConsultarOrdemResponse()
        {
        }
        public BzxConsultarOrdemResponse(string respostaHttp, string codigoOrdem)
        {
            //TODO: Melhorar Filtro
            var orderResponse = JsonConvert.DeserializeObject<BzxConsultarOrdemResponse>(respostaHttp);
            Ordens = orderResponse.Ordens;
            if (Ordens != null)
                Ordem = Ordens.Find(o => o.Codigo == codigoOrdem);
        }
    }
}
