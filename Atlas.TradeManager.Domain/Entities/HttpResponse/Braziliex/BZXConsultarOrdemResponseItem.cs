﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.Braziliex
{
    public class BzxConsultarOrdemResponseItem
    {
        [JsonProperty(PropertyName = "order_number")]
        public string Codigo { get; set; }
        [JsonProperty(PropertyName = "progress")]
        public decimal Progresso { get; set; }
    }
}
