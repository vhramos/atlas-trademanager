﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.Braziliex
{
    public class BzxSaldoResponse
    {
        [JsonProperty(PropertyName = "balance")]
        public BzxSaldoResponseItem Saldos { get; set; }
        public BzxSaldoResponse()
        {
        }
        public BzxSaldoResponse(string respostaHttp)
        {
            var saldoResponse = JsonConvert.DeserializeObject<BzxSaldoResponse>(respostaHttp);
            if (saldoResponse != null)
                Saldos = saldoResponse.Saldos;
        }
    }
}
