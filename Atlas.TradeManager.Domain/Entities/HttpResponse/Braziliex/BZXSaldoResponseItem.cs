﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.Braziliex
{
    public class BzxSaldoResponseItem
    {
        [JsonProperty(PropertyName = "brl")]
        public decimal SaldoBRL { get; set; }
        [JsonProperty(PropertyName = "btc")]
        public decimal SaldoBTC { get; set; }
    }
}
