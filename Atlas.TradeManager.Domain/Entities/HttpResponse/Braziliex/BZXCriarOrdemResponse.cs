﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.Braziliex
{
    public class BzxCriarOrdemResponse
    {
        [JsonProperty(PropertyName = "success")]
        private string _codigoSucesso;
        public bool Sucesso
        {
            get
            {
                return _codigoSucesso == "1";
            }
        }
        [JsonProperty(PropertyName = "message")]
        public string Mensagem { get; set; }
        [JsonProperty(PropertyName = "order_number")]
        public string Codigo { get; set; }

        public BzxCriarOrdemResponse()
        {
        }
        public BzxCriarOrdemResponse(string respostaHttp)
        {
            var orderResponse = JsonConvert.DeserializeObject<BzxCriarOrdemResponse>(respostaHttp);
            _codigoSucesso = orderResponse._codigoSucesso;
            Mensagem = orderResponse.Mensagem;
            Codigo = orderResponse.Codigo;
        }
    }
}
