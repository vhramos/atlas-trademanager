﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BlinkTrade
{
    public class BlkSaldoResponse
    {
        public Dictionary<string, decimal> Moedas { get; set; }
        public BlkSaldoResponse()
        {
        }
        public BlkSaldoResponse(string respostaHttp, string brokerId)
        {
            JToken token = JToken.Parse(respostaHttp)["Responses"][0][brokerId];
            Moedas = JsonConvert.DeserializeObject<Dictionary<string, decimal>>(token.ToString());
        }
    }
}
