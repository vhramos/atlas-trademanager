﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BlinkTrade
{
    public class BlkCriarOrdemResponse
    {
        [JsonProperty("Status")]
        public int Status { get; set; }
        [JsonProperty("Description")]
        public string Mensagem { get; set; }
        [JsonProperty(PropertyName = "Responses")]
        private List<BlkOrdemResponse> OrdemResponses { get; set; }
        public BlkOrdemResponse Ordem { get; set; }
        public bool Sucesso
        {
            get
            {
                return Mensagem == "OK";
            }
        }
        public BlkCriarOrdemResponse()
        {
        }
        public BlkCriarOrdemResponse(string respostaHttp)
        {
            var ordemResponse = JsonConvert.DeserializeObject<BlkCriarOrdemResponse>(respostaHttp);
            OrdemResponses = ordemResponse.OrdemResponses;
            Status = ordemResponse.Status;
            Mensagem = ordemResponse.Mensagem;
            if (OrdemResponses != null && OrdemResponses.FirstOrDefault() != null)
                Ordem = OrdemResponses.First();
        }
    }
}
