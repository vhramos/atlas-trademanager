﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BlinkTrade
{
    public class BlkConsultarOrdemResponseItem
    {
        [JsonProperty("OrdListGrp")]
        public JToken Ordens { get; set; }
    }
}
