﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BlinkTrade
{
    public class BlkConsultarOrdemResponse
    {
        [JsonProperty("Responses")]
        private List<BlkConsultarOrdemResponseItem> OrdemResponses { get; set; }
        public BlkOrdemResponse Ordem { get; set; }
        public BlkConsultarOrdemResponse()
        {
        }
        public BlkConsultarOrdemResponse(string respostaHttp, string codigoOrdem)
        {
            var ordemResponse = JsonConvert.DeserializeObject<BlkConsultarOrdemResponse>(respostaHttp);
            OrdemResponses = ordemResponse.OrdemResponses;
            if (OrdemResponses != null)
            {
                JToken token = OrdemResponses.First().Ordens.First(j => j[0].ToString() == codigoOrdem);
                Ordem = new BlkOrdemResponse()
                {
                    CodigoOrdem = codigoOrdem,
                    StatusOrdem = token[3].ToString()
                };
            }
        }
    }
}
