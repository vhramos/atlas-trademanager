﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.HttpResponse.BlinkTrade
{
    public class BlkOrdemResponse
    {
        [JsonProperty("OrdStatus")]
        public string StatusOrdem { get; set; }
        [JsonProperty("ClOrdID")]
        public string CodigoOrdem { get; set; }
        [JsonProperty("OrdRejReason")]
        public int MotivoRejeicao { get; set; }
    }
}
