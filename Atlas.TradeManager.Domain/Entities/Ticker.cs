﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities
{
    public class Ticker
    {
        public decimal Volumn { get; set; }
        public decimal Last { get; set; }
    }
}
