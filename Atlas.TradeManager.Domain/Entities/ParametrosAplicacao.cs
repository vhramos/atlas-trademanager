﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities
{
    public class ParametrosAplicacao
    {
        public int ParametrosAplicacaoId { get; set; }
        public decimal AlertaVolume { get; set; }
        public decimal AlertaPercentLucro { get; set; }
        public bool EmitirSomAlerta { get; set; }
    }
}
