﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Entities.Exchanges;

namespace Atlas.TradeManager.Domain.Entities
{
    public enum AcaoOrdem
    {
        [Description("Compra")]
        Compra = 0,
        [Description("Venda")]
        Venda = 1
    }
    public enum TipoOrdem
    {
        [Description("Limit")]
        Limit = 0,
        [Description("Market")]
        Market = 1
    }
    public enum StatusOrdem
    {
        [Description("Iniciada")]
        Iniciada = 0,
        [Description("Enviada")]
        Enviada = 1,
        [Description("Processando")]
        Processando = 2,
        [Description("Rejeitada")]
        Rejeitada = 3,
        [Description("Consumida Parcialmente")]
        ConsumidaParcialmente = 4,
        [Description("Consumida")]
        Consumida = 5,
        [Description("Cancelamento Enviado")]
        CancelamentoEnviado = 6,
        [Description("Cancelada")]
        Cancelada = 7,
        [Description("Nova Pendente")]
        NovaPendente = 7,
    }
    public enum MotivosRejeicaoProtocoloFix
    {
        [Description("Broker / Exchange option")]
        BrokerExchangeOption = 0,
        [Description("Unknown symbol")]
        UnknownSymbol = 1,
        [Description("Exchange closed")]
        ExchangeClosed = 2,
        [Description("Order exceeds limit")]
        OrderExceedsLimit = 3,
        [Description("Too late to enter")]
        TooLateToEnter = 4,
        [Description("Unknown Order")]
        UnknownOrder = 5,
        [Description("Duplicate Order")]
        DuplicateOrder = 6,
        [Description("Duplicate of a verbally communicated order")]
        DuplicateVerballyCommunicatedOrder = 7,
        [Description("Stale Order")]
        StaleOrder = 8,
        [Description("Trade Along required")]
        TradeAlongRequired = 9,
        [Description("Invalid Investor ID")]
        InvalidInvestorId = 10,
        [Description("Unsupported order characteristic")]
        UnsupportedOrderCharacteristic = 11,
        [Description("Surveillence Option")]
        SurveillenceOption = 12
    }
    public class Ordem
    {
        public int OrdemId { get; set; }
        public string ExchangeOrdemId { get; set; }
        public string Asset { get; set; }
        public AcaoOrdem AcaoOrdem { get; set; }
        public TipoOrdem TipoOrdem { get; set; }
        public decimal Preco { get; set; }
        public decimal Quantidade { get; set; }
        public virtual Exchange Exchange { get; set; }
        public virtual List<HistoricoOrdem> Historico { get; set; }
        public StatusOrdem StatusAtual
        {
            get
            {
                return Historico.Count > 0 ? Historico.Find(h => h.HistoricoOrdemId == Historico.Max(m => m.HistoricoOrdemId)).StatusOrdem : StatusOrdem.Iniciada;
            }
        }
        public Ordem()
        {
            Historico = new List<HistoricoOrdem>();
        }
    }
}
