﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities
{
    public class RequisicaoHttp
    {
        public string GatewayUrl { get; set; }
        public string RotaApi { get; set; }
        public ICredentials Credencial { get; set; }
        public Dictionary<string, string> Headers { get; set; }
        public Dictionary<string, string> ParametrosQuery { get; set; }
        public Dictionary<string, string> Parametros { get; set; }
        public dynamic JsonBody { get; set; }
        public Method Metodo { get; set; }
        public RequisicaoHttp()
        {
            ParametrosQuery = new Dictionary<string, string>();
            Headers = new Dictionary<string, string>();
            Parametros = new Dictionary<string, string>();
        }
    }
}
