﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.CrossCutting.Extensions;
using Atlas.TradeManager.Domain.Entities.HttpResponse;
using Atlas.TradeManager.Domain.Entities.HttpResponse.BitcoinToYou;

namespace Atlas.TradeManager.Domain.Entities.Exchanges
{
    public class BitcoinToYou : Exchange
    {
        private const string ACAO_COMPRA = "buy";
        private const string ACAO_VENDA = "sell";

        public override RequisicaoHttp CriarRequesicaoNovaOrdem(Ordem ordem)
        {
            var parametros = new Dictionary<string, string>();
            parametros.Add("asset", ordem.Asset);
            parametros.Add("action", ordem.AcaoOrdem == AcaoOrdem.Compra ? ACAO_COMPRA : ACAO_VENDA);
            parametros.Add("amount", ordem.Quantidade.Converter<string>());
            parametros.Add("price", ordem.Preco.Converter<string>());
            var requisicaoOrdem = CriarRequesicaoBase(CriarOrdemResourcePath, parametros);
            requisicaoOrdem.Metodo = Method.POST;
            return requisicaoOrdem;
        }
        public override RequisicaoHttp CriarRequesicaoConsultaHistoricoOrdem(Ordem ordem)
        {
            var parametros = new Dictionary<string, string>();
            parametros.Add("id", ordem.ExchangeOrdemId);
            var requisicaoHistorico = CriarRequesicaoBase(ConsultarOrdemResourcePath, parametros);
            requisicaoHistorico.Metodo = Method.GET;
            return requisicaoHistorico;
        }
        public override RequisicaoHttp CriarRequesicaoConsultaSaldo()
        {
            var requesicaoSaldo = CriarRequesicaoBase(SaldoResourcePath);
            requesicaoSaldo.Metodo = Method.GET;
            return requesicaoSaldo;
        }
        private RequisicaoHttp CriarRequesicaoBase(string rota, Dictionary<string, string> parametros = null)
        {
            RequisicaoHttp ordemRequest = new RequisicaoHttp();
            ordemRequest.GatewayUrl = GatewayUrl;
            ordemRequest.RotaApi = rota;
            ordemRequest.Credencial = CredentialCache.DefaultNetworkCredentials;
            ordemRequest.Headers.Add("nonce", NonceKey);
            ordemRequest.Headers.Add("key", ApiKey);
            ordemRequest.Headers.Add("signature", CriarAssinaturaApi());
            if (parametros != null)
                ordemRequest.ParametrosQuery = parametros;

            return ordemRequest;
        }
        public override void PreencherOrdem(ref Ordem ordem, string respostaHttp)
        {
            var ordemResponse = new B2UOrdemResponse(respostaHttp);
            HistoricoOrdem historico = CriarHistoricoOrdem(ordemResponse, !string.IsNullOrEmpty(respostaHttp) ? respostaHttp : "Valor Mínimo inválido");
            ordem.Historico.Add(historico);
            if (ordemResponse.Ordem != null && ordem.ExchangeOrdemId == null)
                ordem.ExchangeOrdemId = ordemResponse.Ordem.Id;
        }
        private HistoricoOrdem CriarHistoricoOrdem(B2UOrdemResponse ordemResponse, string mensagem)
        {
            return new HistoricoOrdem()
            {
                DataHistorico = DateTime.UtcNow,
                Mensagem = mensagem,
                StatusOrdem = (ordemResponse.Ordem != null ? ConverterStatus(ordemResponse.Ordem.Status) : StatusOrdem.Rejeitada)
            };
        }
        public override void AtualizarSaldo(string respostaHttp)
        {
            var saldos = new B2USaldoResponse(respostaHttp).Saldos;
            if (saldos != null)
            {
                SaldoBRL = saldos.SaldoBRL;
                SaldoBTC = saldos.SaldoBTC;
            }
        }
        private StatusOrdem ConverterStatus(string bitcoinToYouStatus)
        {
            var status = StatusOrdem.Rejeitada;
            switch (bitcoinToYouStatus.ToLower())
            {
                case "open":
                    status = StatusOrdem.Processando;
                    break;
                case "executed":
                    status = StatusOrdem.Consumida;
                    break;
                case "canceled":
                    status = StatusOrdem.Cancelada;
                    break;
            }
            return status;
        }
        private string CriarAssinaturaApi()
        {
            var encoding = new ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(ApiSecret);
            byte[] messageBytes = encoding.GetBytes(NonceKey + ApiKey);
            using (var hmacsha256 = new System.Security.Cryptography.HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage).ToUpper();
            }
        }
    }
}
