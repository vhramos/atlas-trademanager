﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Interfaces.Services;

namespace Atlas.TradeManager.Domain.Entities.Exchanges
{
    //TODO: DECOMPOR ENTIDADE
    public class Exchange
    {
        public int ExchangeId { get; set; }
        public string SiglaExchange { get; set; }
        public string NomeExchange { get; set; }
        [NotMapped]
        public OrderBook OrderBook { get; set; }
        [NotMapped]
        public Ticker Ticker { get; set; }
        [NotMapped]
        public long Ping { get; set; }
        [NotMapped]
        public bool AplicarTaxaSaque { get; set; }
        [NotMapped]
        public bool AplicarTaxaDeposito { get; set; }
        [NotMapped]
        public bool ExecutarOrdensMaker { get; set; }
        [NotMapped]
        public decimal PrecoAsk
        {
            get
            {
                decimal preco = OrderBook.MelhorAsk.Price;
                if (ExecutarOrdensMaker)
                    preco -= (decimal)0.01;
                return preco;
            }
        }
        [NotMapped]
        public decimal PrecoBid
        {
            get
            {
                decimal preco = OrderBook.MelhorBid.Price;
                if (ExecutarOrdensMaker)
                    preco += (decimal)0.01;
                return preco;
            }
        }
        [NotMapped]
        public decimal PrecoRealAsk
        {
            get
            {
                return PrecoAsk * (1 + TaxaTakerDecimal + TaxaDepositoDecimal);
            }
        }
        [NotMapped]
        public decimal PrecoRealBid
        {
            get
            {
                return PrecoBid * (1 - TaxaTakerDecimal - TaxaSaqueDecimal);
            }
        }
        [NotMapped]
        public decimal TaxasAsk
        {
            get
            {
                decimal taxas = 0;
                if (ExecutarOrdensMaker)
                    taxas += TaxaMaker;

                if (AplicarTaxaDeposito)
                    taxas += TaxaDeposito;

                return taxas;
            }
        }
        [NotMapped]
        public decimal TaxasBid
        {
            get
            {
                decimal taxas = 0;
                if (ExecutarOrdensMaker)
                    taxas += TaxaMaker;

                if (AplicarTaxaSaque)
                    taxas += TaxaSaque;
                return taxas;
            }
        }
        [NotMapped]
        public decimal QuantidadeAsk
        {
            get
            {
                return OrderBook.MelhorAsk.Amount;
            }
        }
        [NotMapped]
        public decimal QuantidadeBid
        {
            get
            {
                return OrderBook.MelhorBid.Amount;
            }
        }
        [NotMapped]
        public bool DadosCarregados
        {
            get
            {
                return OrderBook != null && Ticker != null;
            }
        }
        [NotMapped]
        public decimal SaldoBRL { get; set; }
        [NotMapped]
        public decimal SaldoBTC { get; set; }
        public bool ExchangeOnline { get; set; }
        public decimal TaxaMaker { get; set; }
        public decimal TaxaTaker { get; set; }
        public decimal TaxaDeposito { get; set; }
        public decimal TaxaSaque { get; set; }
        [NotMapped]
        public decimal TaxaTakerDecimal
        {
            get
            {
                return TaxaTaker / 100;
            }
        }
        [NotMapped]
        public decimal TaxaDepositoDecimal
        {
            get
            {
                return TaxaDeposito / 100;
            }
        }
        [NotMapped]
        public decimal TaxaSaqueDecimal
        {
            get
            {
                return TaxaSaque / 100;
            }
        }
        public string GatewayUrl { get; set; }
        public string OrderBookResourcePath { get; set; }
        public string OrderBookJsonPath { get; set; }
        public string TickerResourcePath { get; set; }
        public string TickerJsonPath { get; set; }
        public string JsonBidName { get; set; }
        public string JsonAskName { get; set; }
        public string JsonLastName { get; set; }
        public string JsonVolumnName { get; set; }
        public string JsonOrderBookAmountPath { get; set; }
        public string JsonOrderBookPricePath { get; set; }
        public int IntervaloAtualizacao { get; set; }

        public string ApiKey { get; set; }
        public string ApiSecret { get; set; }
        public string ApiPassword { get; set; }
        public string SaldoResourcePath { get; set; }
        public string CriarOrdemResourcePath { get; set; }
        public string ConsultarOrdemResourcePath { get; set; }
        [NotMapped]
        public string NonceKey
        {
            get
            {
                DateTime epochStart = new DateTime(1970, 01, 01, 0, 0, 0, 0, DateTimeKind.Utc);
                TimeSpan timeSpan = DateTime.UtcNow - epochStart;
                return Convert.ToUInt64(timeSpan.TotalSeconds).ToString();
            }
        }
        public int Satoshi
        {
            get
            {
                return (int)Math.Pow(10, 8);
            }
        }
        public virtual int? UltimoIdOrdemLocal { get; set; }
        public virtual string BlinkTradeBokerId { get; set; }
        public virtual RequisicaoHttp CriarRequesicaoNovaOrdem(Ordem ordem)
        {
            throw new NotImplementedException();
        }
        public virtual RequisicaoHttp CriarRequesicaoConsultaSaldo()
        {
            throw new NotImplementedException();
        }
        public virtual RequisicaoHttp CriarRequesicaoConsultaHistoricoOrdem(Ordem ordem)
        {
            throw new NotImplementedException();
        }
        public virtual void AtualizarSaldo(string respostaHttp)
        {
            throw new NotImplementedException();
        }
        public virtual void PreencherOrdem(ref Ordem ordem, string respostaHttp)
        {
            throw new NotImplementedException();
        }
    }
}
