﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.CrossCutting.Extensions;
using Atlas.TradeManager.Domain.Entities.HttpResponse.Braziliex;

namespace Atlas.TradeManager.Domain.Entities.Exchanges
{
    public class Braziliex : Exchange
    {
        private const string ACAO_COMPRA = "buy";
        private const string ACAO_VENDA = "sell";
        public override RequisicaoHttp CriarRequesicaoNovaOrdem(Ordem ordem)
        {
            var parametros = new Dictionary<string, string>();
            parametros.Add("amount", ordem.Quantidade.Converter<string>());
            parametros.Add("price", ordem.Preco.Converter<string>());
            parametros.Add("market", "btc_brl");
            var requisicaoOrdem = CriarRequesicaoBase(CriarOrdemResourcePath, ordem.AcaoOrdem == AcaoOrdem.Compra ? ACAO_COMPRA : ACAO_VENDA, parametros);
            requisicaoOrdem.Metodo = Method.POST;
            return requisicaoOrdem;
        }
        public override RequisicaoHttp CriarRequesicaoConsultaHistoricoOrdem(Ordem ordem)
        {
            var parametros = new Dictionary<string, string>();
            parametros.Add("market", "btc_brl");
            var requisicaoHistorico = CriarRequesicaoBase(ConsultarOrdemResourcePath, "trade_history", parametros);
            requisicaoHistorico.Metodo = Method.POST;
            return requisicaoHistorico;
        }
        public override RequisicaoHttp CriarRequesicaoConsultaSaldo()
        {
            var requesicaoSaldo = CriarRequesicaoBase(SaldoResourcePath, "balance");
            requesicaoSaldo.Metodo = Method.POST;
            return requesicaoSaldo;
        }
        public override void AtualizarSaldo(string respostaHttp)
        {
            var saldos = new BzxSaldoResponse(respostaHttp).Saldos;
            if (saldos != null)
            {
                SaldoBRL = saldos.SaldoBRL;
                SaldoBTC = saldos.SaldoBTC;
            }
        }
        public override void PreencherOrdem(ref Ordem ordem, string respostaHttp)
        {
            StatusOrdem statusOrdem = StatusOrdem.Rejeitada;
            string mensagem = string.Empty;
            if (string.IsNullOrEmpty(ordem.ExchangeOrdemId))
            {
                var ordemResponse = new BzxCriarOrdemResponse(respostaHttp);
                mensagem = ordemResponse.Mensagem;
                if (ordemResponse.Sucesso)
                {
                    statusOrdem = StatusOrdem.Processando;
                    ordem.ExchangeOrdemId = ordemResponse.Codigo;
                }
            }
            else
            {
                var consultaOrdem = new BzxConsultarOrdemResponse(respostaHttp, ordem.ExchangeOrdemId);
                mensagem = string.Concat("Progresso: ", consultaOrdem.Ordem.Progresso);
                statusOrdem = ConverterStatus(consultaOrdem.Ordem.Progresso);
            }
            HistoricoOrdem historico = CriarHistoricoOrdem(mensagem, statusOrdem);
            ordem.Historico.Add(historico);
        }
        private RequisicaoHttp CriarRequesicaoBase(string rota, string comando, Dictionary<string, string> parametros = null)
        {
            RequisicaoHttp ordemRequest = new RequisicaoHttp();
            ordemRequest.GatewayUrl = GatewayUrl;
            ordemRequest.RotaApi = rota;
            if (parametros != null)
                ordemRequest.Parametros = parametros;
            ordemRequest.Parametros.Add("command", comando);
            ordemRequest.Parametros.Add("nonce", NonceKey);
            ordemRequest.Headers.Add("key", ApiKey);
            ordemRequest.Headers.Add("Sign", CriarAssinaturaApi(ordemRequest.Parametros));
            return ordemRequest;
        }
        private string CriarAssinaturaApi(Dictionary<string, string> parametros)
        {
            var secretByte = Encoding.UTF8.GetBytes(ApiSecret);
            var postBytes = Encoding.UTF8.GetBytes(RetornarParametrosPost(parametros));
            var signatureBytes = RetornarHashAssinatura(secretByte, postBytes);
            return BitConverter.ToString(signatureBytes).Replace("-", string.Empty).ToLower();
        }
        private string RetornarParametrosPost(Dictionary<string, string> dctParametros)
        {
            string parametrosPost = string.Empty;
            if (dctParametros.Count > 0)
            {
                foreach (var parametro in dctParametros)
                    parametrosPost += parametro.Key + "=" + parametro.Value + "&";
                parametrosPost = parametrosPost.TrimEnd('&');
            }
            return parametrosPost;
        }
        private byte[] RetornarHashAssinatura(byte[] keyByte, byte[] messageBytes)
        {
            using (var hmacsha512 = new HMACSHA512(keyByte))
            {
                return hmacsha512.ComputeHash(messageBytes);
            }
        }
        private HistoricoOrdem CriarHistoricoOrdem(string mensagem, StatusOrdem statusOrdem)
        {
            return new HistoricoOrdem()
            {
                DataHistorico = DateTime.UtcNow,
                Mensagem = mensagem,
                StatusOrdem = statusOrdem
            };
        }
        private StatusOrdem ConverterStatus(decimal progresso)
        {
            return progresso < 1 ? StatusOrdem.Processando : StatusOrdem.Consumida;
        }
    }
}
