﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.CrossCutting.Extensions;
using Atlas.TradeManager.Domain.Entities.HttpResponse.BitcoinTrade;

namespace Atlas.TradeManager.Domain.Entities.Exchanges
{
    public class BitcoinTrade : Exchange
    {
        private const string ACAO_COMPRA = "buy";
        private const string ACAO_VENDA = "sell";
        private const string TIPO_LIMIT = "limited";
        private const string TIPO_MARKET = "market";

        public override RequisicaoHttp CriarRequesicaoNovaOrdem(Ordem ordem)
        {
            RequisicaoHttp ordemRequest = CriarRequesicaoBase(CriarOrdemResourcePath);
            ordemRequest.Metodo = Method.POST;
            ordemRequest.JsonBody = new
            {
                currency = ordem.Asset,
                type = ordem.AcaoOrdem == AcaoOrdem.Compra ? ACAO_COMPRA : ACAO_VENDA,
                amount = ordem.Quantidade,
                unit_price = ordem.Preco,
                subtype = ordem.TipoOrdem == TipoOrdem.Limit ? TIPO_LIMIT : TIPO_MARKET
            };
            return ordemRequest;
        }
        public override RequisicaoHttp CriarRequesicaoConsultaSaldo()
        {
            var requesicaoSaldo = CriarRequesicaoBase(SaldoResourcePath);
            requesicaoSaldo.Metodo = Method.GET;
            return requesicaoSaldo;
        }
        private RequisicaoHttp CriarRequesicaoBase(string rota, Dictionary<string, string> parametros = null)
        {
            RequisicaoHttp ordemRequest = new RequisicaoHttp();
            ordemRequest.GatewayUrl = GatewayUrl;
            ordemRequest.RotaApi = rota;
            ordemRequest.Headers.Add("Authorization", string.Concat("ApiToken ", ApiKey));
            if (parametros != null)
                ordemRequest.ParametrosQuery = parametros;

            return ordemRequest;
        }
        public override RequisicaoHttp CriarRequesicaoConsultaHistoricoOrdem(Ordem ordem)
        {
            //TODO: FILTRAR MELHOR RANGE DE DATAS ETC
            var parametros = new Dictionary<string, string>();
            parametros.Add("currency", "BTC");
            var requisicaoHistorico = CriarRequesicaoBase(ConsultarOrdemResourcePath, parametros);
            requisicaoHistorico.Metodo = Method.GET;
            return requisicaoHistorico;
        }
        public override void AtualizarSaldo(string respostaHttp)
        {
            var saldos = new BtdSaldoResponse(respostaHttp).ItensSaldo;
            if (saldos != null)
            {
                SaldoBRL = saldos.First(s => s.CodigoMoeda == "BRL").Quantidade;
                SaldoBTC = saldos.First(s => s.CodigoMoeda == "BTC").Quantidade;
            }
        }
        public override void PreencherOrdem(ref Ordem ordem, string respostaHttp)
        {
            StatusOrdem statusOrdem;
            string mensagem;
            if (string.IsNullOrEmpty(ordem.ExchangeOrdemId))
            {
                var ordemResponse = new BtdCriarOrdemResponse(respostaHttp);
                statusOrdem = ordemResponse.Ordem != null ? StatusOrdem.Processando : StatusOrdem.Rejeitada;
                mensagem = ordemResponse.Mensagem;
                if (ordemResponse.Ordem != null)
                    ordem.ExchangeOrdemId = ordemResponse.Ordem.Codigo;
            }
            else
            {
                var consultaOrdem = new BtdConsultarOrdemResponse(respostaHttp, ordem.ExchangeOrdemId);
                mensagem = consultaOrdem.Mensagem;
                statusOrdem = ConverterStatus(consultaOrdem.Ordem.Status);
            }
            HistoricoOrdem historico = CriarHistoricoOrdem(mensagem, statusOrdem);
            ordem.Historico.Add(historico);
        }
        private HistoricoOrdem CriarHistoricoOrdem(string mensagem, StatusOrdem statusOrdem)
        {
            return new HistoricoOrdem()
            {
                DataHistorico = DateTime.UtcNow,
                Mensagem = mensagem,
                StatusOrdem = statusOrdem
            };
        }
        private StatusOrdem ConverterStatus(string statusBtd)
        {
            StatusOrdem status;
            switch (statusBtd.ToLower())
            {

                case "initialized":
                    status = StatusOrdem.Iniciada;
                    break;

                case "waiting":
                    status = StatusOrdem.Processando;
                    break;
                case "executed_partially":
                    status = StatusOrdem.ConsumidaParcialmente;
                    break;
                case "executed_completely":
                    status = StatusOrdem.Consumida;
                    break;
                case "canceled":
                    status = StatusOrdem.Cancelada;
                    break;
                default:
                    status = StatusOrdem.Rejeitada;
                    break;
            }
            return status;
        }
    }
}
