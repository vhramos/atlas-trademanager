﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.CrossCutting.Extensions;
using Atlas.TradeManager.Domain.Entities.HttpResponse.BlinkTrade;

namespace Atlas.TradeManager.Domain.Entities.Exchanges
{
    public class BlinkTrade : Exchange
    {
        private const string ACAO_COMPRA = "1";
        private const string ACAO_VENDA = "2";
        private const string TIPO_LIMIT = "2";
        private const string TIPO_MARKET = "1";
        public override RequisicaoHttp CriarRequesicaoNovaOrdem(Ordem ordem)
        {
            var requisicaoOrdem = CriarRequesicaoBase(CriarOrdemResourcePath);
            requisicaoOrdem.Metodo = Method.POST;
            requisicaoOrdem.JsonBody = new
            {
                MsgType = "D",
                ClOrdID = ordem.ExchangeOrdemId,
                Symbol = "BTCBRL",
                Side = ordem.AcaoOrdem == AcaoOrdem.Compra ? ACAO_COMPRA : ACAO_VENDA,
                OrdType = ordem.TipoOrdem == TipoOrdem.Limit ? TIPO_LIMIT : TIPO_MARKET,
                Price = (long)(ordem.Preco * Satoshi),
                OrderQty = (long)(ordem.Quantidade * Satoshi),
                BrokerID = BlinkTradeBokerId
            };
            return requisicaoOrdem;
        }
        public override RequisicaoHttp CriarRequesicaoConsultaHistoricoOrdem(Ordem ordem)
        {
            var requisicaoHistorico = CriarRequesicaoBase(ConsultarOrdemResourcePath);
            requisicaoHistorico.Metodo = Method.POST;
            requisicaoHistorico.JsonBody = new
            {
                MsgType = "U4",
                OrdersReqID = ordem.ExchangeOrdemId,
                PageSize = 15
            };
            return requisicaoHistorico;
        }
        public override void PreencherOrdem(ref Ordem ordem, string respostaHttp)
        {
            StatusOrdem statusOrdem = StatusOrdem.Rejeitada;
            string mensagem = string.Empty;
            if (ordem.Historico.Count == 0)
            {
                var ordemResponse = new BlkCriarOrdemResponse(respostaHttp);
                mensagem = ordemResponse.Mensagem;
                if (ordemResponse.Ordem != null)
                {
                    statusOrdem = ConverterStatus(ordemResponse.Ordem.StatusOrdem);
                    if (statusOrdem == StatusOrdem.Rejeitada)
                        mensagem = string.Concat("Ordem Rejeitada: ", ((MotivosRejeicaoProtocoloFix)ordemResponse.Ordem.MotivoRejeicao).ToDescription());
                }
            }
            else
            {
                var consultaOrdem = new BlkConsultarOrdemResponse(respostaHttp, ordem.ExchangeOrdemId);
                if (consultaOrdem.Ordem != null)
                    statusOrdem = ConverterStatus(consultaOrdem.Ordem.StatusOrdem);
            }
            HistoricoOrdem historico = CriarHistoricoOrdem(mensagem, statusOrdem);
            ordem.Historico.Add(historico);
        }
        public override RequisicaoHttp CriarRequesicaoConsultaSaldo()
        {
            var requisicaoSaldo = CriarRequesicaoBase(SaldoResourcePath);
            requisicaoSaldo.Metodo = Method.POST;
            requisicaoSaldo.JsonBody = new
            {
                MsgType = "U2",
                BalanceReqID = 1,
            };
            return requisicaoSaldo;
        }
        public override void AtualizarSaldo(string respostaHttp)
        {
            var saldos = new BlkSaldoResponse(respostaHttp, BlinkTradeBokerId).Moedas;
            if (saldos != null)
            {
                SaldoBRL = saldos["BRL"] / Satoshi;
                SaldoBTC = saldos["BTC"] / Satoshi;
            }
        }
        private RequisicaoHttp CriarRequesicaoBase(string rota, Dictionary<string, string> parametros = null)
        {
            RequisicaoHttp ordemRequest = new RequisicaoHttp();
            ordemRequest.GatewayUrl = GatewayUrl;
            ordemRequest.RotaApi = rota;
            string nonce = NonceKey;
            ordemRequest.Headers.Add("Content-Type", "application/json");
            ordemRequest.Headers.Add("Nonce", nonce);
            ordemRequest.Headers.Add("APIKey", ApiKey);
            ordemRequest.Headers.Add("Signature", CriarAssinaturaApi(nonce));
            if (parametros != null)
                ordemRequest.ParametrosQuery = parametros;

            return ordemRequest;
        }
        private string CriarAssinaturaApi(string nonce)
        {
            var secretByte = Encoding.UTF8.GetBytes(ApiSecret);
            var postBytes = Encoding.UTF8.GetBytes(nonce);
            var signatureBytes = RetornarHashAssinatura(secretByte, postBytes);
            return BitConverter.ToString(signatureBytes).Replace("-", string.Empty).ToLower();
        }
        private byte[] RetornarHashAssinatura(byte[] keyByte, byte[] messageBytes)
        {
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                return hmacsha256.ComputeHash(messageBytes);
            }
        }
        private HistoricoOrdem CriarHistoricoOrdem(string mensagem, StatusOrdem statusOrdem)
        {
            return new HistoricoOrdem()
            {
                DataHistorico = DateTime.UtcNow,
                Mensagem = mensagem,
                StatusOrdem = statusOrdem
            };
        }
        private StatusOrdem ConverterStatus(string strStatus)
        {
            StatusOrdem status;
            switch (strStatus.ToLower())
            {
                case "0":
                    status = StatusOrdem.Processando;
                    break;
                case "1":
                    status = StatusOrdem.ConsumidaParcialmente;
                    break;
                case "2":
                    status = StatusOrdem.Consumida;
                    break;
                case "4":
                    status = StatusOrdem.Cancelada;
                    break;
                case "A":
                    status = StatusOrdem.NovaPendente;
                    break;
                default:
                    status = StatusOrdem.Rejeitada;
                    break;
            }
            return status;
        }
    }
}
