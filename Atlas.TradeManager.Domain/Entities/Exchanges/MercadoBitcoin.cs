﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities.Exchanges
{
    public class MercadoBitcoin : Exchange
    {
        public override RequisicaoHttp CriarRequesicaoConsultaSaldo()
        {
            var requesicaoSaldo = CriarRequesicaoBase(SaldoResourcePath);
            requesicaoSaldo.Metodo = Method.POST;
            return requesicaoSaldo;
        }
        public override void AtualizarSaldo(string respostaHttp)
        {
            //aguardando resposta da MercadoBitcoin para finalizar implementação.
        }
        private RequisicaoHttp CriarRequesicaoBase(string rota, Dictionary<string, string> parametros = null)
        {
            RequisicaoHttp ordemRequest = new RequisicaoHttp();
            ordemRequest.GatewayUrl = GatewayUrl;
            ordemRequest.RotaApi = rota;
            string nonce = NonceKey;
            ordemRequest.Parametros.Add("tapi_nonce", nonce);
            ordemRequest.Parametros.Add("tapi_method", "get_account_info");
            ordemRequest.ParametrosQuery.Add("tapi_nonce", nonce);
            ordemRequest.ParametrosQuery.Add("tapi_method", "get_account_info");
            ordemRequest.Headers.Add("TAPI-ID", ApiKey);
            ordemRequest.Headers.Add("TAPI-MAC", CriarAssinaturaApi(nonce));
            ordemRequest.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

            if (parametros != null)
                ordemRequest.ParametrosQuery = parametros;

            return ordemRequest;
        }
        private string CriarAssinaturaApi(string nonce)
        {
            var secretByte = Encoding.UTF8.GetBytes(ApiSecret);
            var postBytes = Encoding.UTF8.GetBytes("/tapi/v3/?tapi_method=get_account_info&tapi_nonce=" + nonce);
            var signatureBytes = RetornarHashAssinatura(secretByte, postBytes);
            return BitConverter.ToString(signatureBytes).Replace("-", string.Empty).ToLower();
        }
        private byte[] RetornarHashAssinatura(byte[] keyByte, byte[] messageBytes)
        {
            using (var hmacsha512 = new HMACSHA512(keyByte))
            {
                return hmacsha512.ComputeHash(messageBytes);
            }
        }
    }
}
