﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Entities.Exchanges;

namespace Atlas.TradeManager.Domain.Entities
{
    public class Trade
    {
        [Key]
        public int TradeId { get; set; }
        public Exchange ExchangeAsk { get; }
        public Exchange ExchangeBid { get; }
        public decimal LucroLiquido
        {
            get
            {
                return (ExchangeBid.PrecoRealBid - ExchangeAsk.PrecoRealAsk) * QtdVendaBTC;
            }
        }
        public decimal PercentualLucroLiquido
        {
            get
            {
                return ((ExchangeBid.PrecoRealBid / ExchangeAsk.PrecoRealAsk) - 1) * 100;
            }
        }
        public decimal TotalTaxas
        {
            get
            {
                return ExchangeAsk.TaxasAsk + ExchangeBid.TaxasBid;
            }
        }
        /// <summary>
        /// Quanto o saldo BRL consegue comprar em BTC.
        /// </summary>
        public decimal PossivelQtdCompraBTC
        {
            get
            {
                return ExchangeAsk.SaldoBRL / ExchangeAsk.PrecoAsk;
            }
        }
        /// <summary>
        /// Quanto o saldo BRL consegue comprar em BTC com a Taxa Taker descontada.
        /// </summary>
        public decimal QtdCompraBTCDescontoTaxa
        {
            get
            {
                return PossivelQtdCompraBTC * (1 - ExchangeAsk.TaxaTakerDecimal);
            }
        }
        /// <summary>
        /// Quantidade que deve ser comprada na exchange com menor ASK.
        /// </summary>
        public decimal QtdCompraBTC
        {
            get
            {
                List<decimal> quantidades = new List<decimal>
                {
                    ExchangeBid.SaldoBTC,
                    ExchangeAsk.QuantidadeAsk,
                    ExchangeBid.QuantidadeBid,
                    PossivelQtdCompraBTC
                };
                return quantidades.Min();
            }
        }
        /// <summary>
        /// Quantidade que deve ser vendida na exchange com o maior BID.
        /// </summary>
        public decimal QtdVendaBTC
        {
            get
            {
                return QtdCompraBTC * (1 - ExchangeAsk.TaxaTakerDecimal);
            }
        }

        public Trade()
        {
        }
        public Trade(Exchange exchangeA, Exchange exchangeB)
        {
            if (exchangeA.PrecoBid > exchangeB.PrecoBid)
            {
                ExchangeBid = exchangeA;
                ExchangeAsk = exchangeB;
            }
            else
            {
                ExchangeBid = exchangeB;
                ExchangeAsk = exchangeA;
            }
        }
    }
}
