﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities
{
    public class CotacaoMoeda
    {
        public decimal Valor { get; set; }
        public DateTime DataCotacaoUTC { get; set; }
    }
}
