﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlas.TradeManager.Domain.Entities
{
    public class HistoricoOrdem
    {
        public int HistoricoOrdemId { get; set; }
        public DateTime DataHistorico { get; set; }
        public StatusOrdem StatusOrdem { get; set; }
        public string Mensagem { get; set; }
        public virtual int OrdemId { get; set; }
    }
}
