﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Entities.Exchanges;

namespace Atlas.TradeManager.Domain.Interfaces.Services
{
    public interface IHistoricoOrdemService : IServiceBase<HistoricoOrdem>
    {
    }
}
