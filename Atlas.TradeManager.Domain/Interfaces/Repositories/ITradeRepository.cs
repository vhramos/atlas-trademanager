﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Entities;

namespace Atlas.TradeManager.Domain.Interfaces.Repositories
{
    public interface ITradeRepository : IRepositoryBase<Trade>
    {
    }
}
