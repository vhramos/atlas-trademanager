﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlas.TradeManager.Domain.Entities;
using Atlas.TradeManager.Domain.Interfaces.Repositories;
using Atlas.TradeManager.Domain.Interfaces.Services;

namespace Atlas.TradeManager.Domain.Services
{
    public class UsuarioService : ServiceBase<Usuario>, IUsuarioService
    {
        public UsuarioService(IUsuarioRepository usuarioRepository)
            : base(usuarioRepository)
        {
        }
    }
}
